<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class System {

    public $token;
    public $errMessage;
    public $errCode;
    public $infos;
    public $validation;

    public function __construct($errorCode = NULL, $errorMessage = NULL, $infos = NULL, $validation = NULL) {
        $this->setData($errorCode, $errorMessage, $infos, $validation);
    }

    public function setData($errorCode = NULL, $errorMessage = NULL, $infos = NULL, $validation = NULL) {
        $this->errCode = $errorCode;
        $this->errMessage = $errorMessage;
        $this->infos = $infos;
        $this->validation = $validation;
    }

}
