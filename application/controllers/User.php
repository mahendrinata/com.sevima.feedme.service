<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends PrivateApiController {

    /**
     * Digunakan untuk mengambil list user
     */
    public function index() {
        $user = $this->model->get_all();

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    /**
     * Digunakan untuk mengambil detail user dengan ID user
     */
    public function detail($id) {
        $user = $this->model->get_by('id', $id);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    /**
     * Digunakan untuk mengambil profil user dan role user yang sedang login
     */
    public function me() {
        $user = $this->model->is_mapper(true)->mapper($this->user);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $user);
    }

    /**
     * Digunakan untuk update profil user
     */
    public function update() {
        $data = $this->postData;
        $data['photo'] = isset($_FILES['photo']) ? $_FILES['photo']['name'] : NULL;
        $update = $this->model->updateData($data['id'],$data);
        if ($update === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, NULL, array('Berhasil Mengupdate User.')));
        } elseif (is_string($update)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $update));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal Mengupdate User.'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR_VALIDATION, NULL, array(), $validation));
            }
        }
        
    }
    
     public function create() {
        $data = $this->postData;
        $data['photo'] = isset($_FILES['photo']) ? $_FILES['photo']['name'] : NULL;
        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, NULL, array('Berhasil Membuat User.')));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat User.'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR_VALIDATION, NULL, array(), $validation));
            }
        }
    }

    /**
     * Digunakan untuk mengganti password user
     */
    public function change_password() {
        $data = $this->postData;
        $update = $this->model->change_password($data['id'],$data);
        if ($update === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, NULL, array('Berhasil Mengganti Password.')));
        } elseif (is_string($update)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $update));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal Mengganti Password.'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR_VALIDATION, NULL, array(), $validation));
            }
        }
    }

    /**
     * Digunakan untuk mereset password (kirim notifikasi ke email)
     */
    public function forgot_password() {
        
    }

    public function list_me() {
        $users = $this->model->order_by('name', 'ASC')->get_many_by(array('id !=' => $this->user['id'], 'status' => Status::ACTIVE));

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $users);
    }

    public function logout() {
        $logout = AuthManager::logout($this->token);
        if ($logout === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, NULL, array('Logout berhasil')));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $logout));
        }
    }

}

?>