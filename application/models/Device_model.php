<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Device_model extends AppModel {

    protected $has_many = array(
        'User_token'
    );

}
